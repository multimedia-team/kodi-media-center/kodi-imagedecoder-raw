# Kodi Media Center language file
# Addon Name: libraw image decoder
# Addon id: imagedecoder.raw
# Addon Provider: Team Kodi
msgid ""
msgstr ""
"Project-Id-Version: KODI Addons\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2023-02-27 20:15+0000\n"
"Last-Translator: Marek Adamski <fevbew@wp.pl>\n"
"Language-Team: Polish <https://kodi.weblate.cloud/projects/kodi-add-ons-image-decoders/imagedecoder-raw/pl_pl/>\n"
"Language: pl_pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.15.2\n"

msgctxt "Addon Summary"
msgid "Reading of RAW digicam images"
msgstr "Odczytywanie obrazów cyfrowych RAW"

msgctxt "Addon Description"
msgid "Addon based on LibRaw for reading RAW files obtained from digital photo cameras (CRW/CR2, NEF, RAF, DNG, MOS, KDC, DCR and others; virtually all RAW formats are supported)."
msgstr "Dodatek oparty na LibRaw do odczytu plików RAW uzyskanych z cyfrowych aparatów fotograficznych (CRW/CR2, NEF, RAF, DNG, MOS, KDC, DCR i innych; obsługiwane są praktycznie wszystkie formaty RAW)."

#. Supported file extension ".3fr" and mimetype "image/3fr" description
msgctxt "#30100"
msgid "Hasselblad 3F RAW Image - Digital photo captured by a Hasselblad digital camera."
msgstr "Obraz RAW Hasselblad 3F - zdjęcie cyfrowe zrobione aparatem cyfrowym Hasselblad."

#. Supported file extension ".arw" and mimetype "image/arw" description
msgctxt "#30101"
msgid "Sony Alpha Raw Digital Camera Image - An ARW file is a digital photograph captured by various Sony digital cameras."
msgstr ""

#. Supported file extension ".cr2" and mimetype "image/cr2" description
msgctxt "#30102"
msgid "Canon Raw Image File - A CR2 file is a raw camera image created by various Canon digital cameras."
msgstr ""

#. Supported file extension ".crw" and mimetype "image/crw" description
msgctxt "#30103"
msgid "Canon Raw CIFF Image File - Uncompressed and unprocessed Camera RAW image file from a Canon digital camera."
msgstr ""

#. Supported file extension ".dcr" and mimetype "image/dcr" description
msgctxt "#30104"
msgid "Kodak Digital Camera RAW Image - DCR file is a digital photograph saved in the Kodak Digital Camera RAW format."
msgstr ""

#. Supported file extension ".dng" and mimetype "image/dng" description
msgctxt "#30105"
msgid "Digital Negative Image File - A DNG file is a RAW camera image saved in the Digital Negative (DNG) format."
msgstr ""

#. Supported file extension ".erf" and mimetype "image/erf" description
msgctxt "#30106"
msgid "Epson RAW File - Camera RAW file captured by an Epson digital camera."
msgstr ""

#. Supported file extension ".kdc" and mimetype "image/kdc" description
msgctxt "#30107"
msgid "Kodak Photo-Enhancer File - Bitmap image format used by several Kodak digital cameras."
msgstr ""

#. Supported file extension ".mdc" and mimetype "image/mdc" description
msgctxt "#30108"
msgid "Minolta Camera Raw Image - An MDC file is a camera raw image created by a Minolta digital camera."
msgstr ""

#. Supported file extension ".mef" and mimetype "image/mef" description
msgctxt "#30109"
msgid "Mamiya RAW Image - Camera RAW image captured by a Mamiya digital camera."
msgstr ""

#. Supported file extension ".mos" and mimetype "image/mos" description
msgctxt "#30110"
msgid "Leaf Camera RAW File - RAW image captured by several different types of digital cameras."
msgstr ""

#. Supported file extension ".mrw" and mimetype "image/mrw" description
msgctxt "#30111"
msgid "Minolta Raw Image File - Raw image format used by Minolta Dimage digital cameras."
msgstr ""

#. Supported file extension ".nef" and mimetype "image/nef" description
msgctxt "#30112"
msgid "Nikon Electronic Format RAW Image - An NEF file is a raw photo captured by a Nikon digital camera."
msgstr ""

#. Supported file extension ".nrw" and mimetype "image/nrw" description
msgctxt "#30113"
msgid "Nikon Raw Image File - Raw image file produced by high-end Nikon COOLPIX and digital SLR cameras."
msgstr ""

#. Supported file extension ".orf" and mimetype "image/orf" description
msgctxt "#30114"
msgid "Olympus RAW File - An ORF file is a RAW photo taken with an Olympus digital camera."
msgstr ""

#. Supported file extension ".pef" and mimetype "image/pef" description
msgctxt "#30115"
msgid "Pentax Electronic File - A PEF file is a raw camera image created by Pentax digital cameras."
msgstr ""

#. Supported file extension ".ppm" and mimetype "image/x-portable-pixmap" description
msgctxt "#30116"
msgid "Portable Pixmap Image File - PPM file is a 24-bit color image formatted using a text format."
msgstr ""

#. Supported file extension ".raf" and mimetype "image/raf" description
msgctxt "#30117"
msgid "Fuji RAW Image File - An RAF file is a digital photograph captured by Fuji digital cameras."
msgstr ""

#. Supported file extension ".raw" and mimetype "image/raw" description
msgctxt "#30118"
msgid "Raw Image Data File - Image generated by digital cameras such as Panasonic, Leica, and Casio."
msgstr ""

#. Supported file extension ".rw2" and mimetype "image/rw2" description
msgctxt "#30119"
msgid "Panasonic RAW Image - An RW2 file is a raw camera image created by a Panasonic LUMIX digital camera."
msgstr ""

#. Supported file extension ".srw" and mimetype "image/srw" description
msgctxt "#30120"
msgid "Samsung RAW Image - Digital photo taken by a Samsung digital camera."
msgstr ""

#. Supported file extension ".srw" and mimetype "image/srw" description
msgctxt "#30121"
msgid "SIGMA X3F Camera RAW File - An X3F file is a RAW file saved in SIGMA's X3F format."
msgstr ""
